const inputValidator = require('../app/inputValidator')

describe('input validator', () => {
  it('should return success', () => {
    const text = "this is a simple valid text"
    const maxCharacters = 20
    const status = inputValidator(text, maxCharacters)
    
    expect(status.hasError).toEqual(false)
  })
  
  it('should return long text error', () => {
    const text = "this is a loooooooooooooooooooooooooooong and invalid text"
    const maxCharacters = 10
    const status = inputValidator(text, maxCharacters)
    
    expect(status.hasError).toEqual(true)
  })
  
  it('should return low max characters value', () => {
    const text = "this is a simple valid text"
    const maxCharacters = 5
    const status = inputValidator(text, maxCharacters)
    
    expect(status.hasError).toEqual(true)
  })
  
  it('should return empty text error', () => {
    const text = ''
    const maxCharacters = 20
    const status = inputValidator(text, maxCharacters)
    
    expect(status.hasError).toEqual(true)
  })
})