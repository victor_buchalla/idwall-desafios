const justify = require('../app/justify')

describe('justify', () => {
  it('should justify given paragraphs', () => {
    const paragraphs = [['This is an simple string'], ['with paragraph and some random content,', 'hello hello bye']]
    const maxCharacters = 40
    const expected = [['This     is     an     simple     string'],
                      ['with  paragraph and some random content,',
                       'hello              hello             bye']]
    
    expect(justify(paragraphs, maxCharacters)).toEqual(expected)
  })
})