const textBeautify = require('../app/textBeautify')

describe('textBeautify', () => {
  it('should beautify given text', () => {
    const text = 'This is an simple string\n\nwith paragraph and some random content, hello hello bye'
    const expected = [ ['This is an simple string'], ['with paragraph and some random content,', 'hello hello bye'] ]
    const maxCharacters = 40

    expect(textBeautify(text, maxCharacters))
      .toEqual(expected)
  })
})