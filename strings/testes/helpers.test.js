const helpers = require('../app/helpers')

describe('helpers', () => {
  it('should split string into array of paragraphs', () => {
    const withParagraph = 'this is an example of string \n\n with paragraph'
    const expectedWithParagraph = ['this is an example of string ',' with paragraph']
    const withoutParagraph = 'this is an example of string without paragraph'
    const expectedWithoutParagraph = ['this is an example of string without paragraph']

    expect(helpers.splitParagraphs(withParagraph)).toEqual(expectedWithParagraph)
    expect(helpers.splitParagraphs(withoutParagraph)).toEqual(expectedWithoutParagraph)
  })
  
  it('should receive nothing and return an empty array (splitParagraphs)', () => {
    expect(helpers.splitParagraphs()).toEqual([])
  })
  
  it('should receive nothing and return an empty array (splitWords)', () => {
    expect(helpers.splitWords()).toEqual([])
  })

  it('should split string into array of words', () => {
    const withWords = 'this is an example of string with words'
    const expectedWithWords = ['this', 'is', 'an', 'example', 'of', 'string', 'with', 'words']
    const withoutWords = undefined
    const expectedWithoutWords = []

    expect(helpers.splitWords(withWords)).toEqual(expectedWithWords)
    expect(helpers.splitWords(withoutWords)).toEqual(expectedWithoutWords)
  })
  
  it('should assoc an item inside an array without mutation', () => {
    const expectedArray = ['any content', 2, 3, 4, 5]
    const immutableArray = [1, 2, 3, 4, 5]
    const array = helpers.replaceInArray(immutableArray, 'any content', 0)

    const isSameArray = array === immutableArray

    expect(array).toEqual(expectedArray)
    expect(isSameArray).toEqual(false)
  })

  it('should remove the first item of the array without mutation', () => {
    const immutableArray = [1, 2, 3, 4, 5]
    const [ firstItem, restArray ] = helpers.removeAndGetFirst(immutableArray)

    expect(immutableArray).toEqual(immutableArray)
    expect(firstItem).toEqual(1)
    expect(restArray).toEqual([2, 3, 4, 5])
  })
  
  it('should create an string with blank spaces', () => {
    const spacesQnty = 4
    const spaces = helpers.createSpaces(spacesQnty)

    expect(spaces).toEqual('    ')
  })
  
  it('should create an string with blank spaces', () => {
    const spacesQnty = 4
    const spaces = helpers.createSpaces(spacesQnty)

    expect(spaces).toEqual('    ')
  })
  
  it('should return the longest word in text', () => {
    const text = 'this is a text with a loooooooooooooooooooong word'
    expect(helpers.getLongestWord(text)).toEqual('loooooooooooooooooooong')
  })
  
  it('should return the empty string', () => {
    const text = ''
    expect(helpers.getLongestWord(text)).toEqual('')
  })
})