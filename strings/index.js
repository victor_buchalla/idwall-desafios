const inquirer = require('inquirer')
const { textQuestion, maxCharactersQuestion, needsJustifyQuestion } = require('./app/questions')
const { toConsole, isInputedTextValid } = require('./app/helpers')
const inputValidator = require('./app/inputValidator')
const textBeatify = require('./app/textBeautify')
const justify = require('./app/justify')

inquirer.prompt(
  [textQuestion, maxCharactersQuestion, needsJustifyQuestion]
)
  .then(({ text, needsJustify, maxCharacters }) => {
    const validation = inputValidator(text, maxCharacters, needsJustify)
    
    if (validation.hasError) {
      return console.log(validation.message)
    }
    
    const beautifierText = textBeatify(text, maxCharacters)
    if (needsJustify) {
      const justifiedText = justify(beautifierText, maxCharacters);
      toConsole(justifiedText)
    } else {
      toConsole(beautifierText)
    }
  })
