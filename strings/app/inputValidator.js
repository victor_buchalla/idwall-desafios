const { getLongestWord, splitWords } = require('./helpers')

const ERRORS = {
  LONGEST_WORD: {
    hasError: true,
    message: 'Existe uma palavra em seu texto com tamanho maior que o numero maximo de caracteres definido'
  },
  MAX_CHARACTERS: {
    hasError: true,
    message: 'Por favor digite um numero maximo de caracteres maior que 10'
  },
  EMPTY_TEXT: {  
    hasError: true,
    message: 'Nenhum texto encontrado'
  },
  SHORT_TEXT: {
    hasError: true,
    message: 'Seu texto deve ter pelo menos 5 palavras'
  }
}

const SUCCESS = {
  hasError: false,
  message: ''
}

const inputValidator = (text, maxCharacters) => {
  if (!maxCharacters || maxCharacters < 10) {
    return ERRORS.MAX_CHARACTERS
  }
  
  if (!text) {
    return ERRORS.EMPTY_TEXT
  }
   
  if(getLongestWord(text).length > maxCharacters) {
    return ERRORS.LONGEST_WORD
  }
  
  if(splitWords(text) < 5) {
    return ERRORS.SHORT_TEXT
  }
    
  return SUCCESS
}

module.exports = inputValidator