const DEFAULT_TEXT = `In the beginning God created the heavens and the earth. Now the earth was formless and empty, darkness was over the surface of the deep, and the Spirit of God was hovering over the waters.

And God said, "Let there be light," and there was light. God saw that the light was good, and he separated the light from the darkness. God called the light "day," and the darkness he called "night." And there was evening, and there was morning - the first day.`

const DEFAULT_MAX_CHARACTERS = 40

const textQuestion = {
  message: 'Digite o texto a ser formatado (será aberto o editor de sua preferência)',
  default: DEFAULT_TEXT,
  type: 'editor',
  name: 'text'
}

const maxCharactersQuestion = {
  message: 'Digite o numero máximo de caracteres',
  default: DEFAULT_MAX_CHARACTERS,
  type: 'number',
  name: 'maxCharacters'
}

const needsJustifyQuestion = {
  message: 'Você gostaria de formatar seu texto no modo "justificado"?',
  default: false, 
  type: 'confirm',
  name: 'needsJustify'
}


module.exports = {
  textQuestion,
  maxCharactersQuestion,
  needsJustifyQuestion
}
