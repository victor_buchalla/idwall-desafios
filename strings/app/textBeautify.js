const {
  splitParagraphs,
  splitWords,
  replaceInArray,
  removeAndGetFirst
} = require('./helpers')

/**
 * Recursive function that receives an array of words
 * and brake it into mutiple lines with a max character rule
 * @param {int} maxCharacters
 * @param {array} words 
 * @param {array} lines 
 * @param {int} lineIndex 
 * @return {array}
 */
const buildLines = (maxCharacters, words, lines = [''], lineIndex = 0) => {
  if (words.length === 0) {
    return lines
  }

  const [currentWord, newWords] = removeAndGetFirst(words)

  if (currentWord.length + lines[lineIndex].length >= maxCharacters) {
    lineIndex++
  }

  const currentLine = lines[lineIndex] || ''

  const lineContent = `${currentLine} ${currentWord}`.trim()

  const newLines = replaceInArray(lines, lineContent, lineIndex)

  return buildLines(maxCharacters, newWords, newLines, lineIndex)
}

/**
 * Splits all words in a paragraph
 * and return an array of lines 
 *
 * @param {int} maxCharacters - High Order Function dependency
 * @param {string} paragraph 
 * @return {array}
 */
const formatEachParagraph = (maxCharacters) => (paragraph) => {
  const words = splitWords(paragraph)
  const lines = buildLines(maxCharacters, words)
  return lines
}

/**
 * Receives an text and return an array of paragraphs with
 * lines inside following the max character rule
 * @param {string} text 
 * @param {int} maxCharacters
 * @return {array}
 */
const textBeautifier = (text, maxCharacters = 40) => {
  const paragraphs = splitParagraphs(text)
  const formatted = paragraphs.map(formatEachParagraph(maxCharacters))
  return formatted
}

module.exports = textBeautifier