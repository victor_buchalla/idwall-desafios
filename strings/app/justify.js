const {
  splitWords,
  createSpaces
} = require('./helpers')

/**
 * Based in how many spaces are missing and if it can spreaded
 * equally between all words it returns the quantity of spaces
 * to be inserted
 * @param {int} missingSpaces 
 * @param {array} words 
 * @return {object}
 */
const getSpacesQnty = (missingSpaces, words) => {
  const spacesBetweenWordsAvaiable = words.length - 1
  if (spacesBetweenWordsAvaiable < 1) {
    return ''
  }
  // if it has the same amout of spaces between all words (excludes last word)
  const hasEqualSpaces = missingSpaces % spacesBetweenWordsAvaiable === 0;
  if (hasEqualSpaces) {
    const equalSpaceQnty = missingSpaces / spacesBetweenWordsAvaiable;
    return {
      even: equalSpaceQnty,
      odd: equalSpaceQnty
    }
  } 
  
  if (missingSpaces < words.length) {
    return {
      even: 1,
      odd: 1
    }
  }
  
  const odd = parseInt(missingSpaces / spacesBetweenWordsAvaiable)
  const even = odd + 1;
  return {
    even,
    odd
  }
    
}

/**
 * Receives a line and justifies it based in maxCharacters
 * 
 * @param {int} maxCharacters - High Order Function dependency
 * @param {string} line
 * @return {string}
 */
const justifyLine = maxCharacters => line => {
  let missingSpaces = maxCharacters - line.length;

  if (missingSpaces === 0)
    return line;

  const words = splitWords(line)
  const spacesQnty = getSpacesQnty(missingSpaces, words)
  
  const addSpacesInEachWord = (word, index, words) => {
    const isLastItem = words.length - 1 === index
    const isEven = index % 2 === 0;

    if (missingSpaces <= 0) {
      return word
    }

    let spaces = isEven ? spacesQnty.even : spacesQnty.odd

    if (spaces > missingSpaces) {
      spaces = missingSpaces
    }

    missingSpaces = missingSpaces - spaces

    return isLastItem ? `${createSpaces(spaces)}${word}` : `${word}${createSpaces(spaces)}`

  }

  const newline = words.map(addSpacesInEachWord).join(' ')
  return newline;
}

/**
 * Format every line in all paragraphs in "justified"
 * 
 * @param {array} paragraphs 
 * @param {int} maxCharacters 
 * @returns {array}
 */
const jusitfy = (paragraphs, maxCharacters) => {
  const justified = paragraphs.map(lines => lines.map(justifyLine(maxCharacters)))
  return justified
}


module.exports = jusitfy