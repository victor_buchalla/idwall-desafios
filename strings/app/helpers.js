const chalk = require('chalk');

const PARAGRAPH = '\n\n'

const BLANK_SPACE = ' '

const LINE = '\n'

const splitParagraphs = (text) => !!text ? text.split(PARAGRAPH) : []

const splitWords = (text) => !!text ? text.split(BLANK_SPACE) : []

const getLongestWord = (text) => {
  const byLength = (a, b) => b.length - a.length
  const words = splitWords(text)
  return words.sort(byLength)[0] || ''
}

const replaceInArray = (array = [], content, index) => Object.assign([], array, { [index]: content });

const removeAndGetFirst = (array = []) => {
  const newArray = [...array];
  const firstItem = newArray.shift()
  return [firstItem, newArray]
}

const createSpaces = (qnt = 0) => isNaN(qnt) ? '' : Array(qnt).fill(BLANK_SPACE).join('')

const toConsole = (paragraphs = []) => {
  const txt = paragraphs.map(lines => lines.join(LINE)).join(PARAGRAPH)
  console.log(PARAGRAPH)
  console.log(chalk.magenta(txt))
}



module.exports = {
  splitParagraphs,
  splitWords,
  replaceInArray,
  removeAndGetFirst,
  createSpaces,
  toConsole,
  getLongestWord
}