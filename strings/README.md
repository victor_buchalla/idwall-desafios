# Instruções

### Node e Yarn
- Instale as dependencias do projeto
```
$ yarn
```

- Rode os testes com
```
$ yarn test
```

- E inicie a aplicação com
```
$ node index
```

### Docker
- Se preferir usar o Docker basta criar a imagem usando
```
$ docker build --rm --file Dockerfile --tag idstrings
```

- E rodar o comando para inicializar a aplicação dentro de um container
```
$ docker run -i -t idstrings node index                            
```

## Observações
Resolvi o teste primeiramente quebrando o text em uma estrutura de paragrafos e linhas que para que fique facil manipular
Na parte de justificação do texto, usei uma solução onde quando os espaços restantes na linha não podem ser dividios igualmente entre as palavras, eu evito colocar 2 grandes adjacentes, sempre dando uma "peso" maior aos espaços de índice par

## Exemplo da aplicação

![strings](https://imgur.com/uQFp854.gif)

