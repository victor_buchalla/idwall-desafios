
const inquirer = require('inquirer')
const cliBot = require('./app/cliBot')
const telegramBot = require('./app/telegramBot')
const { tokenQuestion, initalQuestion, CLI_BOT } = require('./app/questions')

const askTelegramToken = () => inquirer.prompt(tokenQuestion)
  .then(({ token }) => {
    if (!token) {
      return console.error('Token não fornecido ou inválido')
    }

    return token
  })


inquirer.prompt(initalQuestion)
  .then(({ botChoice }) => {
    
    if (botChoice === CLI_BOT)
      return cliBot()
    else 
      return askTelegramToken()
        .then(telegramBot)
    
  })