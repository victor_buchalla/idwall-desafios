
const CLI_BOT = 'Cli bot'
const TELEGRAM_BOT = 'Telegram bot'

const cliBotQuestion = {
  message: 'Digite os subreddits separados por ";" ex: askreddit;worldnews;cats',
  default: 'askreddit;worldnews;cats',
  type: 'input',
  name: 'subreddits'
}

const initalQuestion = {
  message: 'Bem vindo ao bot do reddit, qual bot deseja iniciar ?',
  type: 'list',
  name: 'botChoice',
  choices: [CLI_BOT, TELEGRAM_BOT]
}

const tokenQuestion = {
  message: 'Para iniciar o bot do telegram, por favor insira o token do bot',
  type: 'password',
  name: 'token'
}

module.exports = {
  cliBotQuestion,
  initalQuestion,
  tokenQuestion,
  CLI_BOT,
  TELEGRAM_BOT
}