const request = require('superagent')
const cheerio = require('cheerio')

const MIN_UPVOTES = 5000

/**
 * if thread link is from reddit concat with reddit base url
 * @param  {string} link 
 * @return {string}
 */
const getThreadLink = (link) => {
  const isLinkFromReddit = /^(\/r\/)/.test(link)
  return !isLinkFromReddit ? link : `https://reddit.com${link}`
}

/**
 * Receives thread cheerio object and map
 * it into an simple object structure
 * @param {int} i 
 * @param {obkect} thread 
 */
const mapThreads = (i, thread) => {
  const $thread = cheerio(thread)
  return {
    title: $thread.find('[data-event-action="title"]').text(),
    subreddit: $thread.attr('data-subreddit'),
    score: $thread.attr('data-score'),
    threadLink: getThreadLink($thread.attr('data-url')),
    commentsLink: `https://reddit.com${$thread.attr('data-permalink')}`
  }
}

/**
 * Returns true if thread score is higher or equals MIN_UPVOTES
 * @param {object} thread 
 * @return {boolean}
 */
const byUpvotes = (thread) => ~~thread.score >= MIN_UPVOTES

/**
 * Fetch the subreddit markup and
 * return the relevant threads
 * @param {string} subredditUrl 
 * @param {array}
 */
const crawl = (subredditUrl) => request(`https://old.reddit.com/r/${subredditUrl}`)
  .then(({ text: markup }) => {
    const $dom = cheerio.load(markup)
    const $threads = $dom('[data-promoted="false"]')
    const threads = $threads.map(mapThreads).get()
    const relevantThreads = threads.filter(byUpvotes)
    return relevantThreads
  })
  .catch(() => {
    return []
  })

/**
 * Crawl all given subreddits and returns
 * all threads that are relevants
 * @param {array} subreddits 
 * @param {array} threads 
 */
const crawlMultiple = (subreddits, threads = []) => {
  if (!subreddits || subreddits.length === 0) {
    return Promise.resolve(threads)
  }

  const subreddit = subreddits.shift()
  return crawl(subreddit)
    .then((newThreads) => crawlMultiple([ ...subreddits ], [ ...threads, ...newThreads ]))
}

module.exports = crawlMultiple