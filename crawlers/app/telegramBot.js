const Telegraf = require('telegraf')
const subredditCrawler = require('./subredditCrawler')
const { replyWithWelcome, replyWithError, replyWithThreads } = require('./replies')

/**
 * Handles the command inputted by the user and replies with
 * all subreddits threads found
 * @param {object} ctx 
 */
const handleThreadsCommand = (ctx) => {
  const { message } = ctx
  const { text } = message
  const [_, subredditsString] = text.split(' ')
  if (!subredditsString) {
    return replyWithError(ctx.reply)
  }
  const subreddits = subredditsString.split(';')
  ctx.reply('Estamos buscando as threads pra você!')
  subredditCrawler(subreddits)
    .then(replyWithThreads(ctx.reply))
}

/**
 * Starts telegram bot with Telegraf
 * @param {string} botToken 
 */
const startBot = (botToken) => {
  const bot = new Telegraf(botToken)

  bot.start(replyWithWelcome)

  bot.command(['nadaprafazer', 'NadaPraFazer'], handleThreadsCommand)
  
  console.log('Bot is running....')
  bot.launch()
}

module.exports = startBot