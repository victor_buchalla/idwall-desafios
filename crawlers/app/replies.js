
const replyWithWelcome = (ctx) => ctx.reply(`Bem vindo ao Subreddit Crawler!
Digite "/nadaprafazer" com os subreddits que deseja crawlear separados por ";"
Ex: /nadaprafazer askreddit;worldnews;cats`)


const replyWithError = (reply) => reply(`Desculpe, não entendi
Por favor escreva "/nadaprafazer" com a lista de subreddits separados por ";"
Ex: /nadaprafazer askreddit;worldnews;cats`)


const formatThreadsToMessage = (threads) => threads.map((thread) => `
<b>Título:</b> ${thread.title}
<b>Subreddit:</b> ${thread.subreddit}
<b>Upvotes:</b> ${thread.score}
<b>Thread Link:</b>  <a href="${thread.threadLink}">Clique aqui!</a>
<b>Comentários Link:</b> <a href="${thread.commentsLink}">Clique aqui!</a>
`).join('')

const replyWithThreads = (reply) => (threads) => {
  if (!threads.length) {
    return reply('Desculpe, não encontramos nenhuma thread relevante dentro desses subreddits, tente outros! :)')
  }
  return reply(formatThreadsToMessage(threads), { disable_web_page_preview: true, parse_mode: 'HTML' })
}

module.exports = {
  replyWithWelcome,
  replyWithError,
  replyWithThreads
}