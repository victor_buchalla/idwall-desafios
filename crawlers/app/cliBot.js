const subredditCrawler = require('./subredditCrawler.js')
const chalk = require('chalk')
const inquirer = require('inquirer')
const { cliBotQuestion } = require('./questions')


const log = (category, data) => console.log(chalk.yellow(category) + ' ' + chalk.cyan(data))

/**
 * Logs all thread in console
 * @param {array} threads 
 */
const logThreads = (threads) => {
  threads.forEach(thread => {
    console.log('\n---------------------------\n')
    log('Título:', thread.title)
    log('Subreddit:', thread.subreddit)
    log('Upvotes:', thread.score)
    log('Thread Link:', thread.threadLink)
    log('Comentários Link:', thread.commentsLink)
  })
}

/**
 * Starts bot cli
 */
const startCliBot = () => inquirer.prompt(cliBotQuestion).then(({ subreddits: subredditsString }) => {
  const subreddits = subredditsString && subredditsString.split(';')
  if(!subreddits || !subreddits.length) {
    console.log('Por favor informe os subreddits desejados separados por ";"')
    return null
  }

  subredditCrawler(subreddits)
    .then(logThreads)
})


module.exports = startCliBot