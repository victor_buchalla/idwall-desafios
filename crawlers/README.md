
# Instruções

### Node e Yarn
- Instale as dependencias do projeto
```
$ yarn
```

- Rode os testes com
```
$ yarn test
```

- E inicie a aplicação com
```
$ node index
```

### Docker
- Se preferir usar o Docker basta criar a imagem usando
```
$ docker build --rm --file Dockerfile --tag idcrawlers
```

- E rodar o comando para inicializar a aplicação dentro de um container
```
$ docker run -i -t idcrawlers node index                            
```

## Observações
Resolvi usar um interpretador de markup ao invés de um headless browser por questões de simplicidade em escrever os testes, e performance.
Você pode gerar o próprio token do Bot do telegram com o BotFather ou usar o token que enviei por email junto com o link deste repositorio
Ambos os bots usam o mesmo script pra crawlerar o subreddit, que consiste basicamente em uma recursão que faz um request http e interpreta o markup retornado daquele subreddit, e a partir daquele markup consigo todas as informações necessárias


## Exemplos da aplicação

### Crawler versão cli

![cli bot](https://imgur.com/nJtFh6R.gif)

### Crawler versão telegram

![telegram bot](https://imgur.com/aKP84KM.gif)