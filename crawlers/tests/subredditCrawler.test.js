jest.mock('superagent', () => (subreddit) => new Promise(resolve => {
  if (/notrelevant/gi.test(subreddit)) {
    return resolve({ text: '' });
  }
  const text = `
<div
    data-promoted="false"
    data-subreddit="news"
    data-score="6000"
    data-url="https://google.com/"
    data-permalink="/r/awesome_new_search_engine/">
  <div data-event-action="title">New awesome search engine</div>
</div>
<div
    data-promoted="false"
    data-subreddit="news"
    data-score="10"
    data-url="https://bing.com/"
    data-permalink="/r/regular_new_search_engine/">
  <div data-event-action="title">Regular search engine</div>
</div>
`
  return resolve({ text })
}));

const subredditCrawler = require('../app/subredditCrawler')

describe('subredditCrawler', () => {
  it('should crawl subrredit thread', () => {
    
    const expectedThreads = [
      { 
        title: 'New awesome search engine',
        subreddit: 'news',
        score: '6000',
        threadLink: 'https://google.com/',
        commentsLink: 'https://reddit.com/r/awesome_new_search_engine/'
      }
    ]
    
    return subredditCrawler(['news'])
      .then((threads) => {
        expect(threads).toEqual(expectedThreads)
      })
  })
  
  it('should crawl subrreddit and find no relevant threads', () => {
    return subredditCrawler(['notrelevant'])
      .then((threads) => {
        expect(threads).toEqual([])
      })
  })
  
  it('should receive nothing and returns empty array', () => {
    return subredditCrawler()
      .then((threads) => {
        expect(threads).toEqual([])
      })
  })
})